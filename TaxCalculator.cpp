//============================================================================
// Name        : TaxCalculator.cpp
// Author      : ryvel
// Version     :
// Copyright   : Your copyright notice
// Description : calculate person's taxable income
//============================================================================

#include <iostream>
#include <iomanip>

/**
 * Get person's name, occupation, salary
 * Assume they pay avg tax rate = taxable income
 * Calculate taxable income
 */

class Person
{
	private:
	std::string name;
	unsigned int income;
	unsigned int taxesPaid;

	public:
	Person();
	Person(std::string, unsigned int, unsigned int);

	void setName(std::string newName)
	{
		name = newName;
	}

	void setIncome(unsigned int newIncome)
	{
		income = newIncome;
	}

	void setTaxes(unsigned int newTaxesPaid)
	{
		taxesPaid = newTaxesPaid;
	}

	std::string getName(){
		return name;
	}

	unsigned int getIncome(){
		return income;
	}

	unsigned int getTaxesPaid(){
			return taxesPaid;
		}
};

Person::Person(){
	name = "";
	income =0;
	taxesPaid=0;
}

Person::Person(std::string newName, unsigned int newIncome, unsigned int newTaxesPaid){
	name = newName;
	income = newIncome;
	taxesPaid = newTaxesPaid;
}

double calculateTaxableIncome(Person p)
{	double rate = 0;
	const unsigned int income = p.getIncome();

	if(income < 9700){
		rate = .10;
	}else if(income >= 9700 && income < 39475){
		rate = .12;
	}else if(income >= 39475 && income < 84200){
		rate = .22;
	}else if(income >= 84200 && income < 160725){
		rate = .24;
	}else if(income >= 160725 && income < 204100){
		rate = .32;
	}else{
		rate = .35;
	}

	return (income*rate);
}

double calculateReturn(Person p)
{	double money=0;
	money = (p.getTaxesPaid() - calculateTaxableIncome(p));
	return money;
}

int main() {

	Person p;
	std::string name = "";
	unsigned int income = 0;
	unsigned int taxesPaid = 0;
	//Prompt User
	std::cout << "Enter your name" <<std::endl;
	std::cin >> name;
	std::cout << "Enter your Income" <<std::endl;
	std::cin >> income;
	std::cout << "Enter your taxes paid" <<std::endl;
	std::cin >> taxesPaid;
	//Set stuff
	p.setName(name);
	p.setIncome(income);
	p.setTaxes(taxesPaid);

	double money = calculateReturn(p);
	std::cout << "You got back: " << money ;

	return 0;
}
