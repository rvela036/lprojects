/*
 * Validator.cpp
 *
 *  Created on: Apr 7, 2019
 *      Author: ryanv
 */
#include <iostream>

bool validate(const std::string& cardNum)
{
	int total;
	int num = cardNum.length();
	int temp=0;
	for(int i = num-1 ; i>=0; --i)
	{
		temp = cardNum[i]-'0';
		if(i%2 == 0){
			temp = temp*2;
		}
			total +=temp/10;
			total +=temp%10;
	}
	return (total%10 == 0);
}


int main()
{
	std::string cardNum="";
	std::cout << "Enter card number: "<< std::endl;
	std::cin >> cardNum;
	if(validate(cardNum) != 0)
		std::cout << "Invalid";
	else
		std::cout <<"Valid";
}


